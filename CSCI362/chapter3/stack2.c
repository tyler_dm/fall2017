
/*  Chapter 3 homework, Problem 3.24
 *
 * stack2.c
 *
 * Tyler Manifold
 * CSCI 362
 * Implement 2 stacks using one array
 *
 * NOTE: Do not declare an overflow unless both stacks
 *  are full
 *
 */

#include <stdlib.h>
#include <stdio.h>

#define STACK_REGISTER_SIZE 10

int stack_register[STACK_REGISTER_SIZE];

typedef struct stack {
	char id;
	int top;
	int bottom;
};

struct stack A, B;

// print value and index in register
void stack_info()
{
	printf("Stack info:\n");
	printf("A: [ ");
	for (int i = 0; i < A.top+1; i++)
		printf("%d ", stack_register[i]);
	printf("]\n");
	printf(" A.top: %d, %d\n A.bottom: %d, %d\n\n",
			A.top, stack_register[A.top],
			A.bottom, stack_register[A.bottom]);

	printf("B: [ ");
	for (int i = B.bottom; i > B.top-1; i--)
		printf("%d ", stack_register[i]);
	printf("]\n");
	printf(" B.top: %d, %d\n B.bottom: %d, %d\n\n",
			B.top, stack_register[B.top],
			B.bottom, stack_register[B.bottom]
			);

	printf("Stack register: [ ");
	for (int i = 0; i < STACK_REGISTER_SIZE; i++)
		printf("%d ", stack_register[i]);
	printf("]\n\n");
}

void stack_init()
{
	A.id = 'A';
	A.top = -1;
	A.bottom = 0;

	B.id = 'B';
	B.top = STACK_REGISTER_SIZE;
	B.bottom = STACK_REGISTER_SIZE - 1;

	printf("Initialized stacks.\n");
	stack_info();
}

// push a value onto a stack
// Stack A will push from the front of the register to the center
// Stack B will push from the end of the register to the center
void push(struct stack* s, int value)
{
	printf("Attempting push: %d onto %c[%d]\n", value, s->id, s->top+1);

	if (A.top + 1 == B.top)
		printf("push: Stack register is full! Unable to complete push operation as it will result in an overflow.\n");
	else
	{
		if (s->id == 'A')
		{
			stack_register[++s->top] = value;
		}
		else if (s->id == 'B')
		{
			stack_register[--s->top] = value;
		}
		else
		{
			printf("push: reference to unknown or null stack\n");
		}
	}

	stack_info();
}

// pop top value off of the specified stack and decrement top
int pop(struct stack* s)
{
	int ret_val;
	
	printf("Attempting to pop from %c\n", s->id);


	switch (s->id)
	{
		case 'A':
			if (!(s->top < s->bottom))
			{
				ret_val = stack_register[s->top];
				stack_register[s->top--] = 0;
			}
			break;
		case 'B':
			if (!(s->top > s->bottom))
			{
				ret_val = stack_register[s->top];
				stack_register[s->top++] = 0;
			}
			break;
		default:
			ret_val = 0;
			break;
	}
	
	return ret_val;
}

int main()
{
	printf("Stack register size: %dB\n", sizeof(stack_register));
	
	stack_init();

	for (int i = 0; i < 6; i++)
		push(&A, i);

	for (int i = 10; i >= 5; i--)
		push(&B, i);
	
	for (int i = 0; i < 5; i++)
	{
		printf("%d\n",pop(&A));
		printf("%d\n", pop(&B));
	}

	stack_info();

	return 0;
}
