
//AVLTree.h

#ifndef _AVLTREE_H_
#define _AVLTREE_H_

#include "BTree.h"

class AVLTree : public BTree {

	public:
		void insert(int);
		void print();

	private:
		int max(int, int);
		int height(node*);
		int hdiff(node*);

		void print(node*, int);

		node* insert(int, node*);

		node* balance(node*);

		node* rotateLeftLeft(node*);
		node* rotateRightRight(node*);

		node* rotateLeftRight(node*);
		node* rotateRightLeft(node*);
};

#endif // _AVLTREE_H_
