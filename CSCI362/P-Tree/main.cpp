
/*
 * Tyler Manifold
 * CSCI 362
 * P-Tree
 */

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "BTree.h"
#include "AVLTree.h"


int main()
{

	int choice = -1;

	while (choice < 0)
	{
		std::cout << "Choose a tree to generate from the file:" << std::endl;
		std::cout << "1. AVL Tree" << std::endl;
		std::cout << "2. Binary Search Tree" << std::endl;
		std::cout << "0. Exit" << std::endl;
		std::cout << "> ";

		std::cin >> choice;
	}

	

	switch (choice)
	{
		case 1:
			printf("\n\nCreating AVL Tree.\n\n");

			std::ifstream* avl_file;
			AVLTree* avl;
			
			avl_file = new std::ifstream("treenode.txt");

			if (!avl_file->is_open())
			{
				std::cout << "The file could not be opened!\n";
			}
			else
			{
				avl = new AVLTree();

				std::string val;

				while (avl_file->good())
				{
					getline(*(avl_file), val, ',');

					std::stringstream ss(val);

					int v;

					ss >> v;
					ss.str("");;
					
					avl->insert(v);
					
					avl->print();

					std::cout << "\nPress ENTER to continue.\n";
					std::cin.get();
				
					//std::cout << "Max Height: " << avl->getHeight() << std::endl;
				}

				avl_file->close();
			}

			avl->print();
			
			std::cout << "Press ENTER to nuke the AVL Tree.\n";
			std::cin.get();

			delete avl;
			delete avl_file;

			break;
		case 2:

			std::ifstream* tree_file;

			std::cout << "\nCreating BST\n\n";

			BTree* tree;

			tree_file = new std::ifstream("treenode.txt");

			if (!tree_file->is_open())
			{
				std::cout << "The file could not be opened!\n";
			}
			else
			{
				tree = new BTree();

				std::string val;

				while (tree_file->good())
				{
					getline(*(tree_file), val, ',');

					std::stringstream ss(val);
					
					int v;

					ss >> v;
					ss.str("");

					tree->add(v);
					std::cout << "\nPress ENTER to update.\n";
					std::cin.get();
					tree->print();

					//std::cin.get();
				}	
			}

			tree_file->close();

			tree->print();
			
			std::cout << "Press ENTER to nuke the BST.\n";
			std::cin.get();

			delete tree;
			delete tree_file;

			break;


		default:
			break;
	}

		
	


	return 0;
}

