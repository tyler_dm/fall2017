
// AVLTree.cpp

#include "AVLTree.h"

int AVLTree::max(int a, int b)
{
	return (a >= b) ? a : b;
}

int AVLTree::height(node* n)
{
	return (n == nullptr) ? -1 : n->height;
}

void AVLTree::print()
{
	int depth = 0;

	print(root, depth);
}

void AVLTree::print(node* n, int depth)
{
	if (n->right)
		print(n->right, depth + 1);

	for (int i = 0; i < depth; i++)
		printf("       ");

	if (n != root)
		printf("---");

	if (n == root)
		printf("*");

	printf("%d(%d)\n", n->value, n->height);

	if (n->left)
		print(n->left, depth + 1);
}

void AVLTree::insert(int v)
{
	this->root = this->insert(v, root);
}

node* AVLTree::insert(int v, node* current_node)
{
	if (!current_node)
	{
		current_node  = new node(v);
		return current_node;
	}
	else if (v < current_node->value)
	{
		current_node->left = this->insert(v, current_node->left);
		current_node = this->balance(current_node);
	}
	else if (v >= current_node->value)
	{
		current_node->right = this->insert(v, current_node->right);
		current_node = this->balance(current_node);
	}

	return current_node;

}

// return the difference in height of the left and right subtrees
int AVLTree::hdiff(node *n)
{
	return height(n->left) - height(n->right);
}

// balance the subtree rooted at n
node* AVLTree::balance(node* n)
{
	int balance = hdiff(n);

	if (balance > 1)
	{
		//printf("Checking left side of %d\n", n->value);

		if ( hdiff(n->left) > 0 )
			n = rotateLeftLeft(n);

		else 
			n = rotateLeftRight(n);
		
	}
	else if (balance < -1)
	{
		//printf("Checking right side of %d\n", n->value);

		if( hdiff(n->right) < 0 )
			n = rotateRightRight(n);
		
		else
			n = rotateRightLeft(n);
	}

	n->height = max(height(n->left), height(n->right)) + 1;

	return n;
}

// rotate n to the left
//	  n		    T
//	 / \		   / \
//	A   T	  ->	  n   C
//	   / \	         / \
// 	  B   C		A   B
node* AVLTree::rotateRightRight(node* n)
{
	//printf(" attempting right-right rotation on %d\n",n->value);

	node* t = n->right;
	n->right = t->left;
	t->left = n;
	
	n->height = max( height(n->left), height(n->right) ) + 1;
	t->height = max( height(t->left), height(t->right)) + 1;
/*
	if (n == root)
	{
		root = t;
	}
	else if (root->right = n)
	{
		root->right = t;
	}
	else if (root->left = n)
	{
		root->left = t;
	}
*/
	return t;
	//printf("n: %d, n->left: %d\n", n->value, 
	//		(!n->left) ? -1 : n->left->value);

	//this->print();
	//printf(" done.\n\n");
}

// rotate n to the right
//
// 	    n		  T
//	   / \		 / \
//	  T   C	  ->	A   n
//	 / \		   / \
//	A   B		  B   C
node* AVLTree::rotateLeftLeft(node* n)
{
	//printf(" attempting left-left rotation on subtree rooted at %d\n", n->value);
	

	//print(n, 0);
	//printf("\n");

	node* t = n->left;
	n->left = t->right;
	t->right = n;

	n->height = max( height(n->left), height(n->right) ) + 1;
	t->height = max( height(t->left), n->height ) + 1;

/*
	//printf("root->right: %d\n", root->right->value);
	if (n == root)
	{
		root = t;
	}
	else if (root->right = n)
	{
		root->right = t;
	}
	else if (root->left = n)
	{
		root->left = t;
	}
*/
	return t;
	
	//printf("n: %d, n->left: %d, n->right: %d\n", n->value, 
	//		(!n->left) ? -1 : n->left->value,
	//		(!n->right)? -1 : n->right->value);

	//print(n,0);

	//printf("root->right: %d\n", root->right->value);
	//printf(" done.\n\n");
	//this->print();
}

// double rotate on n
//	    n		  n		  U
//	   / \		 / \	        /   \
//	  T   D	  -> 	U   D	->     T     n
//	 / \	       / \            / \   / \
// 	A   U	      T   C	     A   B C   D
// 	   / \	     / \
// 	  B   C     A   B
node* AVLTree::rotateLeftRight(node* n)
{
	//printf("Performing left-right rotation on %d\n", n->value);
	
	n->left = rotateRightRight(n->left);
	return rotateLeftLeft(n);

	//rotateRightRight(n->left);
	//rotateLeftLeft(n);
}

// double rotate on n
//    n		    n                 U
//   / \	   / \              /   \
//  A   T    ->   A   U	     ->    n     T
//     / \	     / \          / \   / \
/     U   D	    B   T        A   B C   D
//   / \	       / \
//  B   C	      C   D
node* AVLTree::rotateRightLeft(node* n)
{
	//printf("Performing right-left rotation on %d\n", n->value);
	
	n->right = rotateLeftLeft(n->right);
	return rotateRightRight(n);

	//rotateLeftLeft(n->right);
	//rotateRightRight(n);
}
