
// BTree.h

#ifndef _BTREE_H_
#define _BTREE_H_

#include <stdio.h>
#include <stdlib.h>

class node {
	public:
		int value;

		int height;

		node* left;
		node* right;

		node()
		{
			height = -1;
		}

		node(int v)
		{
			value = v;
			height = 0;
			//printf("\nCreated new node:\n  mem: 0x%x\n  val: %d\n", this, value);
		}

		node(node* n)
		{
			this->value = n->value;
			this->height = n->height;
			this->left = n->left;
			this->right = n->right;
		}
};

class BTree {

	protected:
		node* root;

		int data;

		void init();
		void nuke(node*);

	public:
		BTree();
		BTree(int);
		~BTree();
	
		void add(int);
		void add(node*, node*);
		void remove();
	
		void print();
		void print(node*, int);
};

#endif // _BTREE_H_
