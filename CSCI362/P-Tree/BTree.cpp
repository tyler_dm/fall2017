
// BTree.cpp

#include "BTree.h"

BTree::BTree()
{
	this->root = nullptr;
}

BTree::BTree(int v)
{
	this->root->value = v;
}

BTree::~BTree()
{
	printf("--NUKE LAUNCHED--\n");

	this->nuke(root);

	printf("Tree was nuked successfully.\n");
}

void BTree::nuke(node* n)
{
	if (n->right)
		nuke(n->right);
	
	if(n->left)
		nuke(n->left);

	int t = n->value;

	delete n;

	printf("  DELETED NODE: %d\n", t);
}

void BTree::add(int v)
{
	if (!root)
		root = new node(v);
	else
		this->add( new node(v), root );
}

void BTree::add(node* new_node, node* current_node)
{
	if (new_node->value <= current_node->value)
	{
		if (!current_node->left)
		{
			printf("Inserting %d left of %d\n", 
					new_node->value,
					current_node->value);
			current_node->left = new_node;
		}
		else
			this->add(new_node, current_node->left);
	}
	else if (new_node->value > current_node->value)
	{
		if (!current_node->right)
		{
			printf("inserting %d right of %d\n",
					new_node->value,
					current_node->value);
			current_node->right = new_node;
		}
		else
			this->add(new_node, current_node->right);
	}
}

void BTree::print()
{
	//printf("\nPrinting tree. Press ENTER to update:\n\n");

	int depth = 0;

	print(root, depth);
}


void BTree::print(node* n, int depth)
{
	if (n->right)
		print(n->right, depth + 1);
	

	for (int i = 0; i < depth; i++)
		printf("    ");

	
	if (n != root)
		printf("---");

	printf("%d\n", n->value);

	
	if (n->left)
		print(n->left, depth + 1);
}
