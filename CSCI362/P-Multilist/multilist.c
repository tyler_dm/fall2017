
/*
 * Tyler Manifold
 * CSCI 362
 * P-Multilist
 *
 */

#include <stdio.h>
#include <stdlib.h>

#define MAX_CLASS_SIZE 3000
#define MAX_STUDENTS 10000

typedef struct Node {
	int student_number;
	int class_number;

	struct Node* next_student;
	struct Node* next_class;
};

struct Node* head;

// print information about the node
void node_info(const char* name, struct Node* n)
{
	printf("%s: 0x%x, st: %d, cl: %d, next => 0x%x\n", name, n, n->student_number, n->class_number,  n->next_student);
}

void print_list()
{
	struct Node* iter_class = head;
	struct Node* iter_student;

	printf("\nList Contents\n");

	while (iter_class)
	{
		printf("%d,%d\t", iter_class->student_number, iter_class->class_number);

		iter_student = iter_class;
		while(iter_student->next_student)
		{
			iter_student = iter_student->next_student;
			printf("%d,%d\t", iter_student->student_number, iter_student->class_number);
			
		}
		printf("\n");	
		iter_class = iter_class->next_class;
	}
}


// intitialize a node with default values
void node_init(struct Node* n)
{
	n->student_number = 0;
	n->class_number = 0;

	n->next_student = NULL;
	n->next_class = NULL;

	//node_info("init", n);
}

struct Node* node_create()
{
	struct Node* n = malloc(sizeof(struct Node*));

	if (n == NULL)
	{
		printf("node_create: Could not allocate node!\n");
		return NULL;
	}

	node_init(n);

	return n;
}


// insert the node n at the student,class pair. If n is NULL, only perform a search for the pair
struct Node* node_insert(struct Node* n, int student, int class)
{
	// if a node is specified, attempt to insert it into the list
	if (n)
	{
		// search for row and column, if the don't exist create them

		struct Node* iter_student = head;

		// search for the student number until the end of the row is reached, or the desired position is found
		while (iter_student->next_student && iter_student->next_student->student_number < student)
		{
			iter_student = iter_student->next_student;
		}

		// first case, the student number does not exist and is the max value in the list, so we create it
		if (!iter_student->next_student)
		{
			iter_student->next_student = node_create();
			iter_student = iter_student->next_student;
			
			iter_student->student_number = student;

			//node_info("insert", iter_student);
		}
		// second case, the student number does not exist and is between two other values, so we create the node and insert it into the list
		else if (iter_student->student_number < student && student < iter_student->next_student->student_number)
		{
			//printf("is->sn %d  < student %d && student %d < is->ns->sn %d\n", iter_student->student_number, student, student, iter_student->next_student->student_number);
			struct Node* new_st = node_create();
			new_st->student_number = student;

			new_st->next_student = iter_student->next_student;
			iter_student->next_student = new_st;

			//node_info("insert", new_st);
			
		}

		struct Node* iter_class = head;

		// search for the class number until the end of the row is reached, or the desired position is found
		while (iter_class->next_class && iter_class->next_class->class_number < class)
		{
			iter_class = iter_class->next_class;
		}

		// first case, the class number does not exist and is the max value in the list, so we create it
		if (!iter_class->next_class)
		{
			iter_class->next_class = node_create();
			iter_class = iter_class->next_class;

			iter_class->class_number = class;

			//node_info("insert", iter_class);
		}
		// second case, the class number does not exist and is between two other values, so we create the node and insert it into the list
		else if (iter_class->class_number < class && class < iter_class->next_class->class_number)
		{
			//printf("ic->cn %d < class %d && class %d < ic->nc->cn %d\n", iter_class->class_number, class, class, iter_class->next_class->class_number);
			struct Node* new_cl = node_create();

			new_cl->class_number = class;
			new_cl->next_class = iter_class->next_class;
			iter_class->next_class = new_cl;

			//node_info("insert", new_cl);
		}


		iter_student = head;
		iter_class = head;

		// Find the position in the list to insert the node
		// step to the correct student
		while(iter_student && iter_student->student_number < student)
		{	
			iter_student = iter_student->next_student;
		}
	
		// step to the correct class
		while (iter_class && iter_class->class_number < class)
		{
			iter_class = iter_class->next_class;
		}

		//node_info("iter_student", iter_student);
		//node_info("iter_class", iter_class);

		// step right from class
		while (iter_class->next_student && iter_class->next_student->student_number < student)
		{
			iter_class = iter_class->next_student;
		}

		// step down from student
		while (iter_student->next_class && iter_student->next_class->class_number < class)
		{
			iter_student = iter_student->next_class;
		}
		
		
		//node_info("iter_student", iter_student);
		//node_info("iter_class", iter_class);
	

		if (!iter_student->next_class)
		{
			iter_student->next_class = n;
		}
		else if (iter_student->class_number < class && class < iter_student->next_class->class_number)
		{
			n->next_class = iter_student->next_class;
			iter_student->next_class = n;
		}
	

		if (!iter_class->next_student)
		{
			
			iter_class->next_student = n;
		}
		else if (iter_class->student_number < student && student < iter_class->next_student->student_number)
		{
			n->next_student = iter_class->next_student;
			iter_class->next_student = n;
		}

		return NULL;
	}
	else // no node is specified, only search for the given student,class pair
	{
		return NULL;
	}
}

void print_student(int student)
{
	struct Node* st = head;

	while (st->next_student && st->next_student->student_number <= student)
	{
		st = st->next_student;
	}

	if (!st || st->student_number != student)
	{
		printf("Student %d does not exist! Create the student first.\n", student);
	}
	else 
	{
		printf("Classes for student %d:\n", student);
		while (st->next_class)
		{
			printf("%d\n", st->next_class->class_number);
			st = st->next_class;
		}
	}
}

void print_class(int class)
{
	struct Node* cl = head;
	while (cl->next_class && cl->next_class->class_number <= class)
	{
		cl = cl->next_class;
	}

	if (!cl || cl->class_number != class)
	{
		printf("Class %d does not exist! Create the class first!\n", class);
	}
	else
	{
		printf("Students in class %d:\n", class);

		while (cl->next_student)
		{
			printf("%d\n", cl->next_student->student_number);
			cl = cl->next_student;
		}
	}
}

int print_menu()
{
	int c = 0;

	printf("\n1. Input student,class pairs\n");
	printf("2. Print student\n");
	printf("3. Print class\n");
	printf("0. Quit\n> ");

	scanf("%d", &c);

	return c;
}



int main()
{
	// use for loops to create a multidimensional list of nodes of:
	// 3000 classes
	// 10000 students

	printf("Creating root node.\n");
	head = node_create();

	int choice = -1;

	int s = -1;
	int c = -1;

	// choices:
	// 1 enter students
	// 2 display student number
	// 3 display class number
	// 0 quit
	
	while (choice != 0)
	{
		choice = print_menu();	

		switch (choice)
		{
			case 1: // enter student class pair

				printf("Enter a student,class pair: (eg. 4 130). Enter 0 0 to quit.");
				while (s != 0 && c != 0)
				{
					printf("\n> ");
					scanf("%d", &s);
					scanf("%d", &c);

					struct Node* new_st = node_create();
					new_st->student_number = s;
					new_st->class_number  = c;

					node_insert(new_st, s, c);
				}

				s = -1;
				c = -1;

				break;
			
			case 2: // print student

				printf("Student to print: ");
				
				int sp;

				scanf("%d", &sp);

				print_student(sp);

				break;
			
			case 3: // print class

				printf("Class to print: ");

				int cl;

				scanf("%d", &cl);
				
				print_class(cl);

				break;
			
			case 0:
			default:
				break;
		}

	}

	return 0;
}
