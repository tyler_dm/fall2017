
/*
 * Tyler Manifold
 * Data Structures
 * Insertion, Merge, and Quicksort
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define MAX_RANDOM_VALUE 10000
#define SIZE_SMALL 100
#define SIZE_MEDIUM 1000
#define SIZE_LARGE 5000
#define SIZE_LARGEST 10000

void mergeSort(int[], int, int, int*);
void merge(int[], int, int, int, int, int*);
void insertionSort(int[], size_t);
void quickSort(int*, int, int,  int*);

void printData(int[], size_t);
int* intcpy(int*, size_t);
void swap(int*, int*);
int median3(int*, int, int);

double calcCpuTime(clock_t, clock_t);

typedef enum {false, true} bool;

int main()
{
	srand(time(NULL));

	int i;

	// SMALL TESTS

	int* data_small = (int*) malloc(sizeof(int*) * SIZE_SMALL);

	for (i = 0; i < SIZE_SMALL; i++)
	{
		data_small[i] = rand() % MAX_RANDOM_VALUE + 1;
	}

	printf("SMALL LIST -- UNSORTED:\n");
	printData(data_small, SIZE_SMALL);

	// insertion sort small

	printf("Insertion Sort: ");
	
	int* inser_small = intcpy(data_small, SIZE_SMALL);
	
	insertionSort(inser_small, SIZE_SMALL);
	
	printData(inser_small, SIZE_SMALL);

	free(inser_small);

	// merge sort small
	int merge_steps = 0;

	printf("Merge sort: ");
	
	int* merge_small = intcpy(data_small, SIZE_SMALL);
	clock_t merge_start = clock();
	
	mergeSort(merge_small, 0, SIZE_SMALL - 1, &merge_steps);
	
	clock_t merge_end = clock();

	printf("%d elements sorted in %d steps | Execution time %gms\n", SIZE_SMALL,  merge_steps, calcCpuTime(merge_start, merge_end));
	printData(merge_small, SIZE_SMALL);
	
	free(merge_small);

	// quicksort small
	
	printf("Quick sort: ");
	
	int quick_steps = 0;

	int* quick_small = intcpy(data_small, SIZE_SMALL);

	clock_t quick_start = clock();

	quickSort(quick_small, 0, SIZE_SMALL - 1, &quick_steps);

	clock_t quick_end = clock();

	printf("%d elements sorted in %d steps | Execution time %gms\n", SIZE_SMALL,  quick_steps, calcCpuTime(quick_start, quick_end));
	printData(quick_small, SIZE_SMALL);

	free(quick_small);
	free(data_small);

	// MEDIUM TESTS
	int* data_medium = (int*) malloc(sizeof(int*) * SIZE_MEDIUM);

	for (i = 0; i < SIZE_MEDIUM; i++)
	{
		data_medium[i] = rand() % MAX_RANDOM_VALUE + 1;
	}

	// insertion sort medium
	//
	printf("Insertion Sort: ");

	int* inser_medium = intcpy(data_medium, SIZE_MEDIUM);

	insertionSort(inser_medium, SIZE_MEDIUM);
	
	printData(inser_medium, SIZE_MEDIUM);

	free(inser_medium);

	// Merge sort medium
	
	merge_steps = 0;

	printf("Merge sort: ");

	int* merge_medium = intcpy(data_medium, SIZE_MEDIUM);
	merge_start = clock();

	mergeSort(merge_medium, 0, SIZE_MEDIUM - 1, &merge_steps);

	merge_end = clock();

	printf("%d elements sorted in %d steps | Execution time: %gms\n", SIZE_MEDIUM,  merge_steps, calcCpuTime(merge_start, merge_end));
	printData(merge_medium, SIZE_MEDIUM);

	free(merge_medium);

	// quicksort medium
	
	printf("Quick sort: ");
	
	quick_steps = 0;

	int* quick_medium = intcpy(data_medium, SIZE_MEDIUM);

	quick_start = clock();

	quickSort(quick_medium, 0, SIZE_MEDIUM - 1, &quick_steps);

	quick_end = clock();

	printf("%d elements sorted in %d steps | Execution time %gms\n", SIZE_MEDIUM,  quick_steps, calcCpuTime(quick_start, quick_end));
	printData(quick_medium, SIZE_MEDIUM);

	free(quick_medium);	
	free(data_medium);

	// LARGE TESTS
	int* data_large = (int*) malloc(sizeof(int*) * SIZE_LARGE);

	for (i = 0; i < SIZE_LARGE; i++)
	{
		data_large[i] = rand() % MAX_RANDOM_VALUE + 1;
	}

	printf("Insertion Sort: ");

	int* inser_large = intcpy(data_large, SIZE_LARGE);

	insertionSort(inser_large, SIZE_LARGE);

	free(inser_large);

	// Merge sort large
	
	merge_steps = 0;

	printf("Merge sort: ");

	int* merge_large = intcpy(data_large, SIZE_LARGE);
	merge_start = clock();

	mergeSort(merge_large, 0, SIZE_LARGE - 1, &merge_steps);

	merge_end = clock();

	printf("%d elements sorted in %d steps | Execution time: %gms\n", SIZE_LARGE,  merge_steps, calcCpuTime(merge_start, merge_end));

	free(merge_large);

	// Quicksort large
	printf("Quick sort: ");
	
	quick_steps = 0;

	int* quick_large = intcpy(data_large, SIZE_LARGE);

	quick_start = clock();

	quickSort(quick_large, 0, SIZE_LARGE - 1, &quick_steps);

	quick_end = clock();

	printf("%d elements sorted in %d steps | Execution time %gms\n", SIZE_LARGE,  quick_steps, calcCpuTime(quick_start, quick_end));

	free(quick_large);	
	free(data_large);	

	// LARGEST TESTS
	int* data_largest = (int*) malloc(sizeof(int*) * SIZE_LARGEST);

	for (i = 0; i < SIZE_LARGEST; i++)
	{
		data_largest[i] = rand() % MAX_RANDOM_VALUE + 1;
	}

	printf("\nInsertion Sort: ");

	int* inser_largest = intcpy(data_largest, SIZE_LARGEST);

	insertionSort(inser_largest, SIZE_LARGEST);

	free(inser_largest);

	// Merge sort largest
	
	merge_steps = 0;

	printf("Merge sort: ");

	int* merge_largest = intcpy(data_largest, SIZE_LARGEST);
	merge_start = clock();

	mergeSort(merge_largest, 0, SIZE_LARGEST - 1, &merge_steps);

	merge_end = clock();

	printf("%d elements sorted in %d steps | Execution time: %gms\n", SIZE_LARGEST,  merge_steps, calcCpuTime(merge_start, merge_end));
	
	free(merge_largest);

	// quick sort largest
	printf("Quick sort: ");
	
	quick_steps = 0;

	int* quick_largest = intcpy(data_largest, SIZE_LARGEST);

	quick_start = clock();

	quickSort(quick_largest, 0, SIZE_LARGEST - 1, &quick_steps);

	quick_end = clock();

	printf("%d elements sorted in %d steps | Execution time %gms\n", SIZE_LARGEST,  quick_steps, calcCpuTime(quick_start, quick_end));

	free(quick_largest);	
	free(data_largest);	
	
	return 0;
}

// return cpu time in milliseconds
double calcCpuTime(clock_t begin, clock_t end)
{
	return (double) (end - begin) / CLOCKS_PER_SEC * 1000;
}

// print the contents of the array pointed to by int* data.
void printData(int* data, size_t len)
{
	printf("[ ");
	for (int i = 0; i < len; i++)
	{
		printf("%d ", data[i]);
	}
	printf("]");

	printf("\n\n");
}

// creates a new array and copies the contents of the array pointed to by int* arr.
// returns a pointer to the new array
int* intcpy(int* arr, size_t len)
{
	// copy the array to leave the original random assortment intact
	
	size_t arr_size = sizeof(int*) * len;
	
	int* d = (int*) malloc(arr_size);
	
	memcpy(d, arr, arr_size);

	return d;
}

// Perform a merge sort on data[]
void mergeSort(int data[], int start, int end, int* steps)
{	
	int mid;

	if (start < end)
	{
		++(*steps);
		mid = (start + end) / 2;

		mergeSort(data, start, mid, steps);
		mergeSort(data, mid + 1, end, steps);

		merge(data, start, mid, mid + 1, end, steps);
	}
}

// merge 2 subarrays of data[] indicated by start and end variables
void merge(int data[], int start1, int end1, int start2, int end2, int* steps)
{
	int* tmp = intcpy(data, end2 - start1);

	int i = start1; // beginning of first subarray
	int j = start2; // beginning of second subarray
	int k = 0; // index for temporary merging array
	
	while (i <= end1 && j <= end2)
	{
		++(*steps);

		if (data[i] < data[j])
		{
			tmp[k++] = data[i++];
			++(*steps);
		}
		else
		{
			tmp[k++] = data[j++];
			++(*steps);
		}
	}

	// copy remaining elements from first subarray
	while (i <= end1)
	{
		tmp[k++] = data[i++];
		++(*steps);
	}

	// copy remaining elements from second subarray
	while (j <= end2)
	{
		tmp[k++] = data[j++];
		++(*steps);
	}

	// copy elements from tmp back into data
	
	int p, q;
	for (p = start1, q = 0; p <= end2; p++, q++)
	{
		data[p] = tmp[q];
		++(*steps);
	}

	free(tmp);
}

// perform insertion sort on data[]
void insertionSort(int data[], size_t len)
{
	clock_t begin = clock();

	int steps = 0;

	for (int i = 1; i < len; i++) 
	{
		int e = i;

		++steps; // comparison step

		while (e > 0 && data[e] < data[e - 1])
		{
			int tmp = data[e];
			data[e] = data[e - 1];
			data[e - 1] = tmp;
			--e;
			++steps; // shift step
		}
	}

	clock_t end = clock();

	printf("%d elements sorted in %d steps | Execution time: %gms\n", len, steps, calcCpuTime(begin, end));
}

// find the median-of-three for the array pointed to by data and return the position of the pivot
int median3(int* data, int start, int end)
{
	int first = start;
	int last = end - 1;
	int mid = (start + end) / 2;

	//printf("First: %d, middle: %d, last: %d\n", data[first], data[mid], data[last]);
	if (data[first] > data[last])
	{
		//printf("%d > %d\n", data[first], data[last]);
		swap(&data[start], &data[last]);
		//printData(data, end);
	}

	if (data[first] > data[mid])
	{
		//printf("%d > %d\n", data[first], data[mid]);
		swap(&data[mid], &data[start]);
		//printData(data, end);
	}

	if (data[mid] > data[last])
	{
		//printf("%d > %d\n", data[mid], data[last]);
		swap(&data[mid], &data[last]);
		//printData(data, end);
	}
	
	//printData(data, end);

	return mid;
}

// perform a quicksort on data[]
void quickSort(int *data, int left, int right,  int* steps)
{	
	int left_index = left;
	int right_index = right;
	int pivot_index = median3(data, left, right);
	int pivot = data[pivot_index];

	++(*steps); // median3

	// once the pivot is found, swap it to the last index of the array
	//printf("Pivot is %d. swapping to last index.\n", pivot);
	swap(&data[left_index], &data[right]);
	++(*steps);
	//printData(data, right + 1);


	while (left_index <= right_index)
	{
		++(*steps);
		// from the left, find the next element that is larger than pivot
		while (data[left_index] < pivot)
		{
			++left_index;
			++(*steps);
		}

		// from the right, find the next element that is less than the pivot
		while (data[right_index] > pivot)
		{
			--right_index;
			++(*steps);	
		}

		if (left_index <= right_index)
		{
			swap(&data[left_index++], &data[right_index--]);
			++(*steps);		
		}
	}

	if (left < right_index)
	{
		quickSort(data, left, right_index, steps);
		++(*steps);
	}

	if (left_index < right)
	{
		quickSort(data, left_index, right, steps);
		++(*steps);
	}
}

void swap(int *a, int *b)
{
	//printf("Swapped %d, %d\n", *a, *b);
	int tmp = *a;
	*a = *b;
	*b = tmp;
}
