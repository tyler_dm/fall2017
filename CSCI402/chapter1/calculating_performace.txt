
CSCI 402
08.28

Calculating performance

performance(x) = unit of work / execution time
book definition: performance(x) = 1/time(x)

when comparing performance of two machines
	eg. "X is n times faster than Y"

	n = performance(x) / performance(y)

Execution time

elapsed time
- counts everyhting (disk and mem access, i/i, etc)
- a useful number, but often not good for comparison purposes

cpu time
- doesn't count i/o or time spent running other programs
- can be broken up into system time and user time

our focus: User CPU time
- time spent executing the lines of code that are "in" our program (excludes other programs or OS code that may intervene)

performance(a) = n => 1/exec(a) = exec(b) = 25 = 1.25
--------------        ---------   -------   -- 
performance(b)        1/exec(b)   exec(a)   20

Performance ratio => machine A is 1.25 time faster than machine B.



Clocks
------

Sequential Circuits/Finite State Machine

Externtal outputs related to external inputs, but also depend on machine state

What is a clock?
- Essentially a square wave
- An electrical signal with a fixed frequency

Frequency = f
period = T

f = 1/T
T = 1/f

if T = 1ms then there are 1000 Ts in 1 second. T = 1ms or 0.001 second
	Therefore, f= 1/0.001 = 1000Hz

Clock period: duration fo clock cycle
- 250ps = 0.25ns = 250x10^-12s

Clock frequency (rate): Cycles per second
- 4.0GHz = 4000 Mhz = 4.0x10^9 Hz


Seconds   cycles    seconds
------- = ------- x ------ 
Program   program   cycle


1 Hz = 1 Cycle/Second

Clock rate is inverse of clock cycle time
- T = CC = 1/CR = 1/f

Performance can be improved by:
- lowering nnumber of required cycles for a program
- lowering the clock cycle time (increase the clock rate)

different instructions will require different number of instructions

Load instructions: 5 clock cycles
R-Type(Arithmetic) instructions: 4 clock cycles

CPI (Cycles per instruction)
	
	Two concepts
	- Cycles per instruction for a specific instruction: fixed number depending on the design. does not change for a given machine
	- Average cycles per instruction: A statistical measure that may vary from program to program depending on the mixture of instructions


not all instructions take the same amount of time to execute

CPU Cycles for a program = Number of instructions x Avg CPI

Effective CPI
	overall effective CPI is computed by looking at the different types of instructions and their individial cycle counts and averaging

	- CPU CLock cylces = n SUM i = 1: (CPI_i x C_i)
	- Overall effective CPI = CPU Clock cycles/ Total number of instructions

CPU Time = INstuction Count x Average CPI x Clock cycle time
CPU time  = Instruction count x Average CPI
	    ------------------------------
	                clock rate


